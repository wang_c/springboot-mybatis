package com.example.service;

import com.example.entity.CouponItem;
import com.example.entity.CouponItemPlus;
import com.example.factory.CouponItemPlusFactory;
import com.example.mapper.CouponItemMapper;
import org.omg.CORBA.DATA_CONVERSION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CouponItemService {
    @Autowired
    CouponItemMapper couponItemMapper;

    public List<CouponItemPlus> listCouponByUcId(int ucId){
        List<CouponItem> couponItems = couponItemMapper.listCouponByUcId(ucId);
        CouponItemPlusFactory couponItemPlusFactory = new CouponItemPlusFactory();
        /*List<CouponItemPlus> couponItemPluses = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses1 = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses2 = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses3 = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses4 = new ArrayList<>();
        couponItems.forEach(couponItem -> {
            CouponItemPlus couponItemPlus = new CouponItemPlus();
            couponItemPlus.setId(couponItem.getId());
            couponItemPlus.setCouponNo(couponItem.getCouponNo());
            couponItemPlus.setCouponType(couponItem.getCouponType());
            couponItemPlus.setCouponDesc(couponItem.getCouponDesc());
            couponItemPlus.setUcId(couponItem.getUcId());
            couponItemPlus.setValidStatus(couponItem.getValidStatus());
            couponItemPlus.setStartTime(couponItem.getStartTime());
            couponItemPlus.setEndTime(couponItem.getEndTime());
            Date currentDate = new Date();
            long diffDays1 = (couponItem.getEndTime().getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24);
            long diffDays2 = (currentDate.getTime()- couponItem.getStartTime().getTime()) / (1000 * 60 * 60 * 24);
            if(diffDays1 < 7){
                couponItemPlus.setCouponTag("即将过期");
                couponItemPluses1.add(couponItemPlus);
            }else if(diffDays2 < 7){
                couponItemPlus.setCouponTag("新发放");
                couponItemPluses2.add(couponItemPlus);
            }else if(couponItem.getValidStatus() == 0){
                couponItemPlus.setCouponTag("未生效");
                couponItemPluses3.add(couponItemPlus);
            }else{
                couponItemPlus.setCouponTag("普通卡券");
                couponItemPluses4.add(couponItemPlus);
            }
        });
        couponItemPluses.addAll(couponItemPluses1);
        couponItemPluses.addAll(couponItemPluses2);
        couponItemPluses.addAll(couponItemPluses3);
        couponItemPluses.addAll(couponItemPluses4);
        return couponItemPluses;*/
        return couponItemPlusFactory.getCouponItemPlusList(couponItems);
    }
}
