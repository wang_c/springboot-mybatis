package com.example.service;

import com.example.entity.User;
import com.example.mapper.UserMapper;
import com.example.util.PageRequest;
import com.example.util.PageResult;
import com.example.util.PageUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;

import java.util.List;

/**
 * @Author:wangc
 * @Date: 2020/6/24
 * @Time: 14:39
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;

    //通过id查找
    public User SelById(int id){
        return userMapper.SelById(id);
    }

    //通过userName查找
    public List<User> SelByUserName(String userName,int pageNum,int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.SelByUserName(userName);
    }

    //通过realName查找
    public List<User> SelByRealName(String realName,int pageNum,int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.SelByRealName(realName);
    }

    //查找所有用户
    public List<User> findAllUser(int pageNum, int pageSize){
        PageHelper.startPage(pageNum, pageSize);
        return userMapper.selectAllUser();
    }

    /*
    *分页查询
    *@param pageRequest
    *@return
     */
    public PageResult findPage(PageRequest pageRequest){
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    /**
     * 调用分页插件完成分页
     * @param pageRequest
     * @return
     */
    private PageInfo<User> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<User> Menus = userMapper.selectPage();
        return new PageInfo<User>(Menus);
    }

    //添加用户
    public int addUser(User user){
        return userMapper.insert(user);
    }

}
