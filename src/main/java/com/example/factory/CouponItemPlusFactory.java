package com.example.factory;

import com.example.entity.CouponItem;
import com.example.entity.CouponItemPlus;

import java.util.*;

public class CouponItemPlusFactory {
    public List<CouponItemPlus> getCouponItemPlusList(List<CouponItem> couponItems){
        List<CouponItemPlus> couponItemPluses = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses1 = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses2 = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses3 = new ArrayList<>();
        List<CouponItemPlus> couponItemPluses4 = new ArrayList<>();
        couponItems.forEach(couponItem -> {
            CouponItemPlus couponItemPlus = new CouponItemPlus();
            couponItemPlus.setId(couponItem.getId());
            couponItemPlus.setCouponNo(couponItem.getCouponNo());
            couponItemPlus.setCouponType(couponItem.getCouponType());
            couponItemPlus.setCouponDesc(couponItem.getCouponDesc());
            couponItemPlus.setUcId(couponItem.getUcId());
            couponItemPlus.setValidStatus(couponItem.getValidStatus());
            couponItemPlus.setStartTime(couponItem.getStartTime());
            couponItemPlus.setEndTime(couponItem.getEndTime());
            Date currentDate = new Date();
            long diffDays1 = (couponItem.getEndTime().getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24);
            long diffDays2 = (currentDate.getTime()- couponItem.getStartTime().getTime()) / (1000 * 60 * 60 * 24);
            if(diffDays1 < 7){
                couponItemPlus.setCouponTag("即将过期");
                couponItemPluses1.add(couponItemPlus);
            }else if(diffDays2 < 7){
                couponItemPlus.setCouponTag("新发放");
                couponItemPluses2.add(couponItemPlus);
            }else if(couponItem.getValidStatus() == 0){
                couponItemPlus.setCouponTag("未生效");
                couponItemPluses3.add(couponItemPlus);
            }else{
                couponItemPlus.setCouponTag("普通卡券");
                couponItemPluses4.add(couponItemPlus);
            }
        });
        Collections.sort(couponItemPluses1, new Comparator<CouponItemPlus>() {
            @Override
            public int compare(CouponItemPlus o1, CouponItemPlus o2) {
                if(o1.getEndTime().getTime() < o2.getEndTime().getTime()){
                    return -1;
                }else{
                    return 1;
                }
            }
        });
        Collections.sort(couponItemPluses2, new Comparator<CouponItemPlus>() {
            @Override
            public int compare(CouponItemPlus o1, CouponItemPlus o2) {
                if(o1.getStartTime().getTime() < o2.getStartTime().getTime()){
                    return 1;
                }
                else{
                    return -1;
                }
            }
        });
        Collections.sort(couponItemPluses3, new Comparator<CouponItemPlus>() {
            @Override
            public int compare(CouponItemPlus o1, CouponItemPlus o2) {
                if(o1.getStartTime().getTime() < o2.getStartTime().getTime()){
                    return 1;
                }
                else{
                    return -1;
                }
            }
        });
        Collections.sort(couponItemPluses4, new Comparator<CouponItemPlus>() {
            @Override
            public int compare(CouponItemPlus o1, CouponItemPlus o2) {
                if(o1.getStartTime().getTime() < o2.getStartTime().getTime()){
                    return -1;
                }
                else{
                    return 1;
                }
            }
        });
        couponItemPluses.addAll(couponItemPluses1);
        couponItemPluses.addAll(couponItemPluses2);
        couponItemPluses.addAll(couponItemPluses3);
        couponItemPluses.addAll(couponItemPluses4);
        return couponItemPluses;
    }
}
