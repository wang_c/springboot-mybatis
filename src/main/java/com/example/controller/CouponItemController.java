package com.example.controller;

import com.example.service.CouponItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@RestController
@RequestMapping("/coupon")
public class CouponItemController {
    @Autowired
    CouponItemService couponItemService;
    @PostMapping("/listCoupon/{ucId}")
    public Object listCouponByUcId(@PathVariable int ucId){
        return couponItemService.listCouponByUcId(ucId);
    }

    public static void main(String[] args) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(df.format(new Date()));
        System.out.println(df.format(new Date()).getClass());
        SimpleDateFormat df2 = new SimpleDateFormat( "yyyy-MM-dd 23:59:59");
        String endTime = df2.format(new Date());
        System.out.println(endTime);
        Timestamp end_date = Timestamp.valueOf(endTime);
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_MONTH, 1);

        Date sDate = c.getTime();
        System.out.println("Date结束日期+1 " +df.format(sDate));
        try{
            Date sDate1 = df.parse(df.format(new Date()));
            System.out.println(sDate1);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
