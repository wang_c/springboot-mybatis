package com.example.controller;

import com.example.entity.User;
import com.example.mapper.UserMapper;
import com.example.service.UserService;
import com.example.util.PageRequest;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author:wangc
 * @Date: 2020/6/24
 * @Time: 14:39
 */

@RestController
@RequestMapping("/testBoot")
public class UserController {

    @Autowired
    private UserService userService;

    //通过id查询
    @RequestMapping("getUserById/{id}")
    public String GetUser(@PathVariable int id){
        return userService.SelById(id).toString();
    }

    //通过userName查询，在url中输入参数
    @RequestMapping(value = "/getUserByUserName/{userName}/{pageNum}/{pageSize}", produces = {"application/json;charset=UTF-8"})
    public Object GetUserByUserName(@PathVariable String userName,@PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize){
        return userService.SelByUserName(userName,pageNum,pageSize);
    }

    //通过realName查询，在url中输入参数
    @RequestMapping(value = "/getUserByRealName/{realName}/{pageNum}/{pageSize}", produces = {"application/json;charset=UTF-8"})
    public Object GetUserByRealName(@PathVariable String realName,@PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize){
        return userService.SelByRealName(realName,pageNum,pageSize);
    }

    //分页查询
    @PostMapping(value="/findPage")
    public Object findPage(@RequestBody PageRequest pageQuery) {
        return userService.findPage(pageQuery);
    }

    //查找所有用户，在url中输入参数
    @ResponseBody
    @RequestMapping(value = "/all/{pageNum}/{pageSize}", produces = {"application/json;charset=UTF-8"})
    public Object findAllUser(@PathVariable("pageNum") int pageNum, @PathVariable("pageSize") int pageSize){

        return userService.findAllUser(pageNum,pageSize);
    }

    //添加user
    @ResponseBody
    @RequestMapping(value = "/add", produces = {"application/json;charset=UTF-8"})
    public int addUser(){
        User user = new User();
        user.setPassWord("123456");
        user.setUserName("abc");
        user.setRealName("abc");
        user.setStartTime(new Date());
        return userService.addUser(user);
    }


    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(date);
    }
}
