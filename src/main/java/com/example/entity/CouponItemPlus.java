package com.example.entity;

import lombok.Data;

@Data
public class CouponItemPlus extends CouponItem {

    //券标签
    private String couponTag;
}
