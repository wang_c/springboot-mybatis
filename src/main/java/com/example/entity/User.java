package com.example.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author:wangc
 * @Date: 2020/6/24
 * @Time: 14:39
 */
@Data
public class User {
    private Integer id;
    private String userName;
    private String passWord;
    private String realName;
    private Date startTime;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", passWord='" + passWord + '\'' +
                ", realName='" + realName + '\'' +
                ", startTime='" + startTime + '\'' +
                '}';
    }
    public String toString1(){
        return "aaa";
    }
}
