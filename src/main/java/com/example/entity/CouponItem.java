package com.example.entity;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * 券列表
 */
@Data
@ToString
public class CouponItem {

    private int id;

    private String couponNo;

    private int couponType;

    private String couponDesc;
    //经纪人id
    private int ucId;

    //券状态
    private int validStatus;

    private Date startTime;

    private Date endTime;
}
