package com.example.mapper;

import com.example.entity.CouponItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CouponItemMapper {
    List<CouponItem> listCouponByUcId(int ucId);
}
