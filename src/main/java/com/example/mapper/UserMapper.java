package com.example.mapper;

import com.example.entity.User;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Author:wangc
 * @Date: 2020/6/24
 * @Time: 14:39
 */
@Repository
public interface UserMapper {

    //通过id查询
    User SelById(int id);

    //通过userName查询
    List<User> SelByUserName(String userName);

    //通过realName查询
    List<User> SelByRealName(String realName);

    //插入user
    int insert(User record);

    int insertSelective(User record);

    //查找全部user
    List<User> selectAllUser();

    //分页查询
    List<User> selectPage();
}
