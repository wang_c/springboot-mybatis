package com.example.demo;

import com.example.entity.User;
import com.example.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	@Autowired
	UserService userService;

	@Test
	public void contextLoads() {
		//assertEquals(1,1);
		List<User> list = userService.findAllUser(5,2);
		for(User user:list){
			System.out.println(user.getUserName());
		}
		//System.out.println(userService.SelByUserName("w",1,2));
	}

}
