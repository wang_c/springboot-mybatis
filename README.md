<pre class="code highlight" lang="markdown"><span id="LC1" class="line" lang="markdown">项目结构如下：</span>
<span id="LC2" class="line" lang="markdown">其中mvnw、mvnw.cmd是maven构建生成。src目录下的都是spring boot项目规范文件。</span>
<span id="LC3" class="line" lang="markdown">Springboot_Mybatis   ---项目名</span>
<span id="LC5" class="line" lang="markdown">     src</span>
<span id="LC6" class="line" lang="markdown">          main</span>
<span id="LC7" class="line" lang="markdown">                java</span>
<span id="LC8" class="line" lang="markdown">                    com.example                  ---根目录（包名）</span>
<span id="LC9" class="line" lang="markdown">                        config                  ---配置信息类</span>
<span id="LC10" class="line" lang="markdown">                        controller              ---前端控制器</span>
<span id="LC11" class="line" lang="markdown">                        entity                  ---实体类</span>
<span id="LC12" class="line" lang="markdown">                        mapper                  ---映射接口</span>
<span id="LC13" class="line" lang="markdown">                        service                 ---数据服务层（impl)</span>
<span id="LC14" class="line" lang="markdown">                        util                    ---工具类</span>
<span id="LC15" class="line" lang="markdown">                        DemoApplication.java     ---工程启动类</span>
<span id="LC16" class="line" lang="markdown">                resources           ---资源文件</span>
<span id="LC17" class="line" lang="markdown">                    mapping             ---映射类</span>
<span id="LC18" class="line" lang="markdown">                        UserMapper.xml           ---映射文件的sql语句</span>
<span id="LC19" class="line" lang="markdown">                    application.yml         ---配置文件（替代application.propertities）</span>
<span id="LC20" class="line" lang="markdown">                    application-dev.yml     ---开发环境配置文件</span>
<span id="LC21" class="line" lang="markdown">                    banner.txt              ---项目启动时显示的字符（随便改）</span>
<span id="LC22" class="line" lang="markdown">          test   ---测试</span>
<span id="LC24" class="line" lang="markdown">     .gitignore    ---版本控制需要忽略的文件</span>
<span id="LC26" class="line" lang="markdown">     mvnw          ---存放maven-wrapper.properties和相关jar包</span>
<span id="LC27" class="line" lang="markdown">     mvnw.cmd      ---执行mvnw命令的cmd入口</span>
<span id="LC28" class="line" lang="markdown">     pow.xml       ---springboot配置文件（管理相关依赖）  </span>
<span id="LC29" class="line" lang="markdown">rebase测试1 </span>
<span id="LC29" class="line" lang="markdown">rebase测试11 </span>
</pre>
